use flate2::read::ZlibDecoder;
use std::fmt::{Debug, Display};
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fs;
use std::io::{Read, Write};

use crate::nbt::{
  NBTFile,
  NBTError,
  TagWriter,
};
use crate::compression::compress;

use super::{
  SpatiallyBounded,
  PosNotInContainerError,
};
use super::chunk::{
  Chunk,
  GetBlockError as ChunkGetBlockError,
  SetBlockError as ChunkSetBlockError,
};

pub struct Region {
  pub chunks: HashMap<(u8, u8), Chunk>,
  pub region_x: i32,
  pub region_z: i32,
  filename: String,
  raw_bytes: Vec<u8>,
  location_data: Vec<Vec<ChunkLocation>>,
  modified: bool,
}

#[derive(Debug)]
pub struct ChunkLocation {
  sector_offset: u32,
  sector_count: u8,
}

pub enum GetChunkError {
  MissingError(u8, u8),
  LoadError(u8, u8, NBTError),
}

#[derive(Debug)]
pub enum BlockError<T> where T: Display + Debug {
  BlockNotInRegionError(PosNotInContainerError),
  GetChunkError(GetChunkError),
  ChunkError(T),
}

pub type GetBlockError = BlockError<ChunkGetBlockError>;

pub type SetBlockError = BlockError<ChunkSetBlockError>;

impl Region {

  pub fn new(region_dir: &str, region_x: i32, region_z: i32) -> Result<Region, std::io::Error> {
    let filename = format!("{}/r.{}.{}.mca", region_dir, region_x, region_z);
    let bytes = fs::read(&filename)?;
    let location_data: Vec<Vec<ChunkLocation>> = (0..32)
      .map(|z| (0..32)
        .map(|x| ChunkLocation::parse(&bytes[(4 * (z * 32 + x))..]))
        .collect())
      .collect();
    Ok(Region {
      raw_bytes: bytes,
      location_data,
      chunks: HashMap::new(),
      region_x,
      region_z,
      filename,
      modified: false,
    })
  }

  pub fn is_modified(&self) -> bool {
    self.modified
  }

  pub fn mark_modified(&mut self) {
    self.modified = true;
  }

  pub fn save(&mut self) -> Result<(), std::io::Error> {
    if self.modified {
      for ((chunk_x, chunk_z), chunk) in self.chunks.drain() {
        if chunk.is_modified() {
          let location = &self.location_data[chunk_z as usize][chunk_x as usize];
          let start = (location.sector_offset as usize) * 4096;
          let max_length = (location.sector_count as usize) * 4096;
          let src_buf = TagWriter::write(&chunk.into());
          let dst_buf = &mut self.raw_bytes[(start + 5)..(start + max_length)];
          let size = compress(&src_buf, dst_buf)? as u32;
          let size_bytes = size.to_be_bytes();
          for i in 0..4 {
            self.raw_bytes[start + i] = size_bytes[i];
          }
        }
      }
      let mut file = fs::File::create(&self.filename)?;
      file.write_all(&self.raw_bytes)?;
    }
    self.modified = false;
    Ok(())
  }

  pub fn is_chunk_loaded(&self, x: u8, z: u8) -> bool {
    self.chunks.contains_key(&(x, z))
  }

  pub fn is_chunk_present(&self, x: u8, z: u8) -> bool {
    self.location_data[z as usize][x as usize].sector_offset != 0
  }

  pub fn load_chunk(&mut self, chunk_x: u8, chunk_z: u8) -> Result<&mut Chunk, GetChunkError> {
    let key = (chunk_x, chunk_z);
    if self.chunks.contains_key(&key) {
      Ok(self.chunks.get_mut(&key).unwrap())
    } else if self.is_chunk_present(chunk_x, chunk_z) {
      let chunk = self.load_chunk_worker(chunk_x, chunk_z)
        .map_err(|e| GetChunkError::LoadError(chunk_x, chunk_z, e))?;
      self.chunks.insert(key, chunk);
      Ok(self.chunks.get_mut(&key).unwrap())
    } else {
      Err(GetChunkError::MissingError(chunk_x, chunk_z))
    }
  }

  fn load_chunk_worker(&self, chunk_x: u8, chunk_z: u8) -> Result<Chunk, NBTError> {
    // Load chunk by going to chunk binary offset, decompressing, and Parsing NBT
    let location = &self.location_data[chunk_z as usize][chunk_x as usize];
    // let mut buffer: [u8; 500000] = [0; 500000]; // TODO: Need a better method than this buffer
    let mut buffer = vec![0 as u8; 500000];
    let start = (location.sector_offset * 4096) as usize;
    let size = u32::from_be_bytes(self.raw_bytes[start..(start + 4)].try_into().unwrap()) as usize;
    let compression_type = self.raw_bytes[start + 4];
    let compressed = &self.raw_bytes[(start + 5)..(start + 5 + size - 1)];
    match ZlibDecoder::new(compressed).read(&mut buffer[0..]) {
      Ok(num_read) => {
        if num_read == buffer.len() {
          panic!("ChunkReader buffer exceeded decompressed chunk size!");
        } else {
          let chunk_nbt = std::panic::catch_unwind(|| {
            NBTFile::from(&buffer[0..num_read]).unwrap()
          }).map_err(|_| {
            NBTError::UnexpectedTag(format!("Caught a panic while reading chunk at location {:?}, starting at byte {} with size {} and compression_type {}", location, start, size, compression_type))
          })?;
          Ok((chunk_nbt, chunk_x, chunk_z).into())
        }
      },
      Err(e) => Err(NBTError::DecompressionError(e)),
    }
  }

  pub fn load_chunks(&mut self, chunks: &[[u8; 2]]) {
    for [x, z] in chunks.iter() {
      if let Err(e) = self.load_chunk(*x, *z) {
        eprintln!("Error: while loading chunk ({}, {})", x, z);
        eprintln!("     : {}", String::from(e).replace("\n", "\n  "));
      }
    }
  }

  pub fn load_all_chunks(&mut self, verbose: bool) {
    for x in 0..32 {
      for z in 0..32 {
        if verbose {
          println!("Loading chunk {}, {}", x, z);
        }
        if let Err(e) = self.load_chunk(x, z) {
          eprintln!("Error: while loading chunk ({}, {})", x, z);
          eprintln!("     : {}", String::from(e).replace("\n", "\n  "));
        }
      }
    }
  }

  pub fn get_chunk(&mut self, chunk_x: u8, chunk_z: u8) -> Result<&Chunk, GetChunkError> {
    self.load_chunk(chunk_x, chunk_z).map(|c| &*c)
  }

  pub fn get_chunk_mut(&mut self, chunk_x: u8, chunk_z: u8) -> Result<&mut Chunk, GetChunkError> {
    self.load_chunk(chunk_x, chunk_z)
  }

  pub fn get_palette(&mut self, verbose: bool) -> HashSet<String> {
    if verbose {
      println!("Loading all chunks");
    }
    self.load_all_chunks(verbose);
    if verbose {
      println!("Searching chunk palettes");
    }
    self.chunks.iter().map(|(_, chunk)| {
      chunk.get_palette()
    }).flatten().collect()
  }

  pub fn get_block_at<'a>(&'a mut self, x: i32, y: u8, z: i32) -> Result<Option<String>, GetBlockError> {
    self.check_contains_pos(x, y, z)
      .map_err(|e| BlockError::BlockNotInRegionError(e))?;
    let chunk_x = ((x - (self.region_x * 16 * 32)) / 16) as u8;
    let chunk_z = ((z - (self.region_z * 16 * 32)) / 16) as u8;

    let chunk = self.get_chunk(chunk_x, chunk_z)
      .map_err(|e| BlockError::GetChunkError(e))?;
    chunk.get_block_at(x, y, z)
      .map_err(|e| BlockError::ChunkError(e))
  }

  pub fn set_block_at(&mut self, x: i32, y: u8, z: i32, name: &str) -> Result<(), SetBlockError> {
    self.check_contains_pos(x, y, z)
      .map_err(|e| BlockError::BlockNotInRegionError(e))?;
    let chunk_x = ((x - (self.region_x * 16 * 32)) / 16) as u8;
    let chunk_z = ((z - (self.region_z * 16 * 32)) / 16) as u8;

    let chunk = self.get_chunk_mut(chunk_x, chunk_z)
      .map_err(|e| BlockError::GetChunkError(e))?;
    chunk.set_block_at(x, y, z, name)
      .map_err(|e| BlockError::ChunkError(e))?;
    self.modified = true;
    Ok(())
  }

  pub fn find_blocks(&mut self, name: &str) -> Vec<&Chunk>{
    self.load_all_chunks(false);
    self.chunks.iter()
      .filter_map(|(_, chunk)| {
        if chunk.contains_block(name) {
          Some(chunk)
        } else {
          None
        }
      })
      .collect()
  }

  pub fn replace_blocks(&mut self, find: &str, replace: &str) {
    self.load_all_chunks(false);
    for (_, chunk) in self.chunks.iter_mut() {
      chunk.replace_blocks(find, replace);
    }
    self.modified = true;
  }
}

impl std::fmt::Display for Region {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Region ({}, {})", self.region_x, self.region_z)
  }
}

impl SpatiallyBounded for Region {

  fn get_lower_bounds(&self) -> (i32, u8, i32) {
    let x = (self.region_x) * 16 * 32;
    let z = (self.region_z) * 16 * 32;
    let y = 0;
    (x, y, z)
  }

  fn get_upper_bounds(&self) -> (i32, u8, i32) {
    let x = (self.region_x + 1) * 16 * 32;
    let z = (self.region_z + 1) * 16 * 32;
    let y = 255;
    (x, y, z)
  }

}

impl std::fmt::Display for GetChunkError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      GetChunkError::MissingError(x, z) => {
        write!(f, "Chunk ({}, {}) has not been generated by the game yet", x, z)
      },
      GetChunkError::LoadError(chunk_x, chunk_z, cause) => {
        write!(f, "Chunk ({}, {}) failed to load: {:?}", chunk_x, chunk_z, cause)
      },
    }
  }
}

impl std::fmt::Debug for GetChunkError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{}", self)
  }
}

impl From<GetChunkError> for String {
  fn from(e: GetChunkError) -> String {
    format!("{}", e)
  }
}

impl ChunkLocation {
  fn parse(bytes: &[u8]) -> ChunkLocation {
    ChunkLocation {
      sector_offset: u32::from_be_bytes([0, bytes[0], bytes[1], bytes[2]]),
      sector_count: bytes[3]
    }
  }
}

impl<T> Display for BlockError<T> where T: Display + Debug {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      BlockError::GetChunkError(e) => write!(f, "{}", e),
      BlockError::ChunkError(e) => write!(f, "{}", e),
      BlockError::BlockNotInRegionError(e) => write!(f, "{}", e),
    }
  }
}
