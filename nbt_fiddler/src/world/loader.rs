use crate::nbt::{Tag, TagWriter};
use crate::util;

use super::chunk::{
  Chunk,
  Level,
};
use super::section::{
  Palette,
  Section,
};

impl From<(Tag, u8, u8)> for Chunk {

  fn from((mut chunk_nbt, chunk_x, chunk_z): (Tag, u8, u8)) -> Chunk {
    let data = chunk_nbt.as_compound_mut().unwrap();
    let level_nbt = data.pop("Level").unwrap();
    let level = Level::from(level_nbt);
    Chunk::new(chunk_nbt, level, chunk_x, chunk_z)
  }

}

impl From<Chunk> for Tag {

  fn from(chunk: Chunk) -> Tag {
    let mut tag = chunk.chunk_nbt;
    tag.as_compound_mut().unwrap().insert("Level", chunk.level.into()).unwrap();
    tag
  }

}

impl From<Tag> for Level {

  fn from(mut level_nbt: Tag) -> Self {
    let data = level_nbt.as_compound_mut().unwrap();
    let x_pos = data.pop("xPos").unwrap().as_int().unwrap();
    let z_pos = data.pop("zPos").unwrap().as_int().unwrap();
    let sections = data.pop("Sections")
      .unwrap()
      .into_list()
      .unwrap()
      .into_iter()
      .map(|section_tag| section_tag.into())
      .collect();
    Level::new(level_nbt, sections, x_pos, z_pos)
  }

}

impl From<Level> for Tag {

  fn from(level: Level) -> Tag {
    let mut tag = level.level_nbt;
    let data = tag.as_compound_mut().unwrap();
    data.insert("xPos", Tag::Int(level.x_pos)).unwrap();
    data.insert("zPos", Tag::Int(level.z_pos)).unwrap();
    data.insert("Sections", Tag::List(
      level.sections.into_iter().map(|s| s.into()).collect(),
      10)).unwrap();
    tag
  }

}

impl From<Tag> for Section {

  fn from(mut section_nbt: Tag) -> Self {
    let data = section_nbt.as_compound_mut().unwrap();
    let y = data.pop("Y").unwrap().as_byte().unwrap();
    let palette = data.pop("Palette")
      .map(|palette| {
        palette.into_list().unwrap().into_iter().map(|p| p.into()).collect()
      });
    let block_states = data.pop("BlockStates")
      .map(|block_states| {
        let longs = block_states.into_long_array().unwrap();
        util::PackedLongArray::new_with_length(longs, 4096)
      });
    Section::new(section_nbt, y, block_states, palette)
  }
}

impl From<Section> for Tag {

  fn from(section: Section) -> Tag {
    let mut tag = section.section_nbt;
    let data = tag.as_compound_mut().unwrap();
    data.insert("Y", Tag::Byte(section.y)).unwrap();
    if let Some(palette) = section.palette {
      data.insert("Palette", Tag::List(
        palette.into_iter().map(|p| p.into()).collect(),
        10)).unwrap();
    }
    if let Some(block_states) = section.block_states {
      data.insert("BlockStates", Tag::LongArray(block_states.into_longs())).unwrap();
    }
    tag
  }

}

impl From<Tag> for Palette {

  fn from(mut palette_nbt: Tag) -> Palette {
    let data = palette_nbt.as_compound_mut().unwrap();
    let name = data.pop("Name").unwrap().into_string().unwrap();
    Palette::new(palette_nbt, name)
  }
}

impl From<Palette> for Tag {

  fn from(palette: Palette) -> Tag {
    let mut tag = palette.palette_nbt;
    let data = tag.as_compound_mut().unwrap();
    data.insert("Name", Tag::String(palette.name)).unwrap();
    tag
  }

}

impl Chunk {

  pub fn tag_round_trip_test(nbt_bytes: &[u8], chunk_x: u8, chunk_z: u8) {
    let old = nbt_bytes.to_vec();
    let nbt: Tag = Tag::from(nbt_bytes).unwrap();
    let chunk: Chunk = (nbt, chunk_x, chunk_z).into();
    let chunk_tag: Tag = chunk.into();
    let new = TagWriter::write(&chunk_tag);

    if new.len() != old.len() {
      println!("Round trip tag binary changed");
    } else {
      for i in 0..new.len() {
        if new[i] != old[i] {
          println!("Round trip tag binary changed (at pos: {})", i);
          println!("Old scan up to 20:");
          util::scan(&old, 20);
          println!("New scan up to 20:");
          util::scan(&new, 20);
          panic!()
        }
      }
      println!("Round trip tag binary intact");
    }
  }
}